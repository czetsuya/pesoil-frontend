# Pesoil Frontend

The front end web application.

### Libraries
- Material UI - UI Framework
- Axios - HTTP requests
- React Query - Query Handling and States
- React Router DOM - Routing / Navigation