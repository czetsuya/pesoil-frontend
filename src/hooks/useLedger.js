import { useState, useEffect } from "react";

const LEDGER_KEY = "pesoil:ledgers";
const useLedger = () => {
  const localLedgers = JSON.parse(localStorage.getItem(LEDGER_KEY) || "[]");
  const [ledgerList, setLedgerList] = useState(localLedgers);

  useEffect(() => {
    if (ledgerList && ledgerList instanceof Array) {
      localStorage.setItem(LEDGER_KEY, JSON.stringify(ledgerList));
    }
  }, [ledgerList]);

  const createLedger = (newLedger) => {
    setLedgerList((oldLedgerList) => [...oldLedgerList, newLedger]);
  };

  const endLedger = (
    id,
    { expenses, loans, endDate, harvestSales, harvestUnit, harvestAmount }
  ) => {
    const updatedLedgers = ledgerList.map((ledger) => {
      if (ledger.id === id) {
        ledger.endDate = endDate;
        ledger.harvestUnit = harvestUnit;
        ledger.harvestAmount = harvestAmount;
        ledger.harvestSales = harvestSales;
        ledger.expenses = expenses;
        ledger.loans = loans;
        ledger.profit = harvestSales - loans - expenses;
      }
      return ledger;
    });
    setLedgerList(updatedLedgers);
  };

  const getActiveLedger = (ledgerId) => {
    return ledgerList.map(e => {
      if (e.id === ledgerId)
        return e;
    })[0];
  }

  return { ledgerList, createLedger, endLedger, getActiveLedger };
};

export default useLedger;
