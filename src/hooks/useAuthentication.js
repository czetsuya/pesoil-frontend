import React, { useCallback, useEffect, useLayoutEffect, useState } from 'react'

import { Auth, Hub } from 'aws-amplify'
import { signInButton, signInButtonContent } from '@aws-amplify/ui'

import * as AuthorizationActions from '../redux/modules/Authorization'

/**
 * Handle user authentication and related features.
 *
 * @returns {{
 *   isAuthenticated: boolean,
 *   user: CognitoUser,
 *   error: any,
 *   signIn: function,
 *   signOut: function,
 *   SignInButton: React.Component,
 * }}
 *
 * @see https://aws-amplify.github.io/amplify-js/api/classes/authclass.html
 * @see https://aws-amplify.github.io/amplify-js/api/classes/hubclass.html
 * @see https://aws-amplify.github.io/docs/js/hub#listening-authentication-events
 * @see https://github.com/aws-amplify/amplify-js/blob/master/packages/amazon-cognito-identity-js/src/CognitoUser.js
 * @see https://github.com/aws-amplify/amplify-js/issues/5284
 * @see https://github.com/aws-amplify/amplify-js/issues/3640
 * @see https://medium.com/better-programming/build-a-react-app-with-authentication-using-aws-amplify-49db1dfdc290
 * @see https://medium.com/@georgemccreadie/introduction-to-using-aws-cognito-hosted-ui-with-amplify-js-4711cf4f925a
 */

const USER_KEY = 'pesoil:user'
const useAuthentication = ({dispatch}) => {
  const localUser = localStorage.user ? JSON.parse(localStorage.user) : null
  const [user, setUser] = useState(localUser)
  const [isAuthenticated, setIsAuthenticated] = useState(!!localUser)
  const [error, setError] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const refreshState = useCallback(() => {
    setIsLoading(true)

    Auth.currentAuthenticatedUser()
      .then(user => {
        localStorage.setItem(USER_KEY, JSON.stringify(user))
        setUser(user)
        setIsAuthenticated(_isAuthenticated(user))
        setError(null)
        setIsLoading(false)
        dispatch(AuthorizationActions.updateToken({
          token: user.signInUserSession.accessToken.jwtToken,
          externalReferenceId: user.username, 
          email: user.attributes.email, 
          firstName: user.attributes.given_name, 
          lastName: user.attributes.family_name}))
      })
      .catch(err => {
        console.log(err);
        setIsAuthenticated(false)
        if (err === 'not authenticated') {
          setError(null)
        } else {
          setError(err)
        }
        setIsLoading(false)
        dispatch(AuthorizationActions.logout())
      })
  }, [dispatch])

  // Make sure our state is loaded before first render
  useLayoutEffect(() => {
    if (!user) {
      refreshState()
    }
  }, [refreshState, user])

  // Subscribe to auth events
  useEffect(() => {
    const handler = ({ payload }) => {
      switch (payload.event) {
        case 'configured':
        case 'signIn':
        case 'signIn_failure':
        case 'signIn_success':
        case 'signOut':
          refreshState()
          break

        default:
          break
      }
    }

    Hub.listen('auth', handler)

    return () => {
      Hub.remove('auth', handler)
    }
  }, [refreshState])

  const signIn = useCallback(() => {
    Auth.federatedSignIn(/*{ provider: 'COGNITO' }*/).catch(err => {
      setError(err)
    })
  }, [])

  const signOut = useCallback(() => {
    localStorage.setItem(USER_KEY, undefined);
    localStorage.setItem('visited', undefined);
    Auth.signOut()
      .then(_ => refreshState())
      .catch(err => {
        setError(err)
      })
  }, [refreshState])

  const CognitoSignInButton = useCallback(
    ({ label = 'Sign In' }) => (
      <button className={signInButton} onClick={signIn}>
        <span className={signInButtonContent}>{label}</span>
      </button>
    ),
    [signIn]
  )

  return {
    isAuthenticated,
    isLoading,
    user,
    error,
    signIn,
    signOut,
    SignInButton: CognitoSignInButton,
  }
}

const _isAuthenticated = user => {
  if (
    !user ||
    !user.signInUserSession ||
    !user.signInUserSession.isValid ||
    !user.signInUserSession.accessToken ||
    !user.signInUserSession.accessToken.getExpiration
  ) {
      return false;
    }

  const session = user.signInUserSession
  const isValid = session.isValid() || false

  const sessionExpiry = new Date(session.accessToken.getExpiration() * 1000)
  const isExpired = new Date() > sessionExpiry

  return isValid && !isExpired
}

export default useAuthentication