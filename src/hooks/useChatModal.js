import React, { useState, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import Coconut from "../assets/coconut.svg";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  modalContainer: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    minWidth: "100%",
    padding: "20px",
    boxSizing: "border-box",
  },
  modal: {
    backgroundColor: "transparent",
    minWidth: "300px",
    borderRadius: "5px",
    padding: "10px",
    "&:focus": {
      outline: "unset",
    },
  },
  cocoSvg: {
    maxWidth: "120px",
  },
  chatContainer: {
    backgroundColor: "white",
    padding: "20px",
  },
  svgContainer: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    display: "flex",
  },
  speechBubble: {
    position: "relative",
    background: "white",
    borderRadius: ".4em",
    marginBottom: "20px",
    padding: "20px",
    fontSize: "20px",
    "&::focus": {
      outline: "unset",
    },
    "&::before": {
      content: '""',
      position: "absolute",
      bottom: 0,
      left: "60%",
      width: 0,
      height: 0,
      border: "20px solid transparent",
      borderTopColor: "white",
      borderBottom: 0,
      borderRight: 0,
      marginLeft: "-10px",
      marginBottom: "-20px",
    },
  },
}));

const useChatModal = () => {
  const [open, setOpen] = useState(false);
  const [activeContent, setActiveContent] = useState(null);
  const [params, setCocoParams] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    if (!activeContent) {
      console.log("x");
      setOpen(false);
    }
  }, [activeContent]);
  const getContent = () => {
    switch (activeContent) {
      case "EQSP":
        return (
          <>
            Hi Dela Cruz Family!
            <br />
            <br />I noticed you are spending {params[0]} more on equipment
            rental than your average monthly total of {params[1]}, would you
            like to learn how to save more money?
            <Grid container style={{ paddingTop: "20px" }}>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  style={{ fontSize: "20px" }}
                  onClick={() => setActiveContent("EQSPYes")}
                >
                  Yes
                </Button>
              </Grid>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="secondary"
                  style={{ fontSize: "20px" }}
                  onClick={() => {
                    setActiveContent(null);
                  }}
                >
                  No Thanks
                </Button>
              </Grid>
            </Grid>
          </>
        );
        case "WEATHER":
          return (
            <>
              Hi Dela Cruz Family!
              <br />
              <br />The official weather channel is reporting anticipated heavy rainfall near your location (Pampanga) on Saturday, Aug 22, 2020. This has a medium risk of impacting your crop of {params[0]}. Would you like more information?
              <Grid container style={{ paddingTop: "20px" }}>
                <Grid item xs={12} style={{ margin: "10px 10px" }}>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    style={{ fontSize: "20px" }}
                    onClick={null}
                  >
                    See what I can do
                  </Button>
                </Grid>
                <Grid item xs={12} style={{ margin: "10px 10px" }}>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="secondary"
                    style={{ fontSize: "20px" }}
                    onClick={() => {
                      setActiveContent(null);
                    }}
                  >
                    No Thanks
                  </Button>
                </Grid>
              </Grid>
            </>
          );
      case "EQSPYes":
        return (
          <>
            Here are the options for equipment rental in your area
            <br />
            <br />
            <ul>
              <li>Monar Equipment Corporation</li>
              <li>Double A Tractor Parts</li>
              <li>Kerpi Tractor Philippines</li>
            </ul>
            <Grid container style={{ paddingTop: "20px" }}>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  style={{ fontSize: "20px" }}
                  onClick={null}
                >
                  See Average Prices
                </Button>
              </Grid>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="secondary"
                  style={{ fontSize: "20px" }}
                  onClick={() => {
                    setActiveContent(null);
                  }}
                >
                  Close
                </Button>
              </Grid>
            </Grid>
          </>
        );
      case "PESTYES":
        return (
          <>
            Here are some pesticide options:
            <br />
            <br />
            <ul>
              <li>Dr. Earth Final Stop</li>
              <li>Sevin Zero Pests CX 50</li>
              <li>Spectracide Malathion</li>
            </ul>
            <Grid container style={{ paddingTop: "20px" }}>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  style={{ fontSize: "20px" }}
                  onClick={() => setActiveContent("PESTTIPS")}
                >
                  See what else I can do
                </Button>
              </Grid>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="secondary"
                  style={{ fontSize: "20px" }}
                  onClick={() => {
                    setActiveContent(null);
                  }}
                >
                  Close
                </Button>
              </Grid>
            </Grid>
          </>
        );
      case "PESTTIPS":
        return (
          <>
            Here are some pest control tips:
            <br />
            <br />
            <ul>
              <li>Rodent-proof each structure</li>
              <li>Good Cleaning and Clearing Practices</li>
              <li>Using Traps</li>
              <li>Regular Monitoring for Pest Activity</li>
              <li>Employ Pest control service</li>
            </ul>
            <Grid container style={{ paddingTop: "20px" }}>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="secondary"
                  style={{ fontSize: "20px" }}
                  onClick={() => {
                    setActiveContent(null);
                  }}
                >
                  Close
                </Button>
              </Grid>
            </Grid>
          </>
        );
      case "PEST":
        return (
          <>
            Hi Dela Cruz Family!
            <br />
            <br />
            There has been an increase of reports from farmers in your location
            (Pampanga) of the pest Aphid for the crop you are planting (
            {params[0]}). Would you like more information?
            <Grid container style={{ paddingTop: "20px" }}>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  style={{ fontSize: "20px" }}
                  onClick={() => setActiveContent("PESTYES")}
                >
                  See pesticide options
                </Button>
              </Grid>
              <Grid item xs={12} style={{ margin: "10px 10px" }}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="secondary"
                  style={{ fontSize: "20px" }}
                  onClick={() => {
                    setActiveContent(null);
                  }}
                >
                  No Thanks
                </Button>
              </Grid>
            </Grid>
          </>
        );
      default:
        return null;
    }
  };
  const CocoModal = ({ activeId, params, action }) => {
    return (
      <Modal
        open={open && activeContent}
        onClose={() => setOpen(false)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        className={classes.modalContainer}
      >
        <div className={classes.modal}>
          <div className={classes.speechBubble}>
            {getContent(params, action)}
          </div>
          <div className={classes.svgContainer}>
            <img
              src={Coconut}
              className={classes.cocoSvg}
              alt="Coco the Coconut"
            />
          </div>
        </div>
      </Modal>
    );
  };

  return {
    CocoModal,
    setOpen,
    setActiveContent,
    setCocoParams,
  };
};

export default useChatModal;
