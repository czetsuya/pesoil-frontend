import { useState, useEffect } from "react";

const TRANSACTIONS_KEY = "pesoil:transactions";
const BUDGET_KEY = "pesoil:budget";

export const CATEGORY = {
  1: "Food / Drink",
  2: "Utility / Bills",
  3: "School / Tuition",
  4: "Others",
  5: "Pesticide",
  6: "Herbicide",
  7: "Insecticide",
  8: "Maintenance / Cleaning",
  9: "Labor",
  10: "Renting of tools",
  11: "Seeds",
  12: "Fertilizers",
  13: "Others",
  99: "Loan Payment",
};

const DEFAULT_BUDGET = {
  1: {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
  },
  2: {
    5: 0,
    6: 0,
    7: 0,
    8: 0,
    9: 0,
    10: 0,
    11: 0,
    12: 0,
    13: 0,
  },
};

const groupByTransactionType = (transactions, wallet) =>
  transactions.reduce(
    (cur, transaction) => {
      return {
        debit:
          transaction.type === "debit" && wallet === transaction.purposeId
            ? (cur.debit += transaction.value)
            : cur.debit,
        credit:
          transaction.type === "credit" && wallet === transaction.purposeId
            ? (cur.credit += transaction.value)
            : cur.credit,
      };
    },
    { credit: 0, debit: 0 }
  );

const useTransaction = (ledgerId, wallet) => {
  const LEDGER_TRANSACTION_KEY = `${TRANSACTIONS_KEY}:${ledgerId}`;
  const LEDGER_BUDGET_KEY = `${BUDGET_KEY}:${ledgerId}`;
  const localTransactions = JSON.parse(
    localStorage.getItem(LEDGER_TRANSACTION_KEY) || "[]"
  );
  const localBudget = JSON.parse(
    localStorage.getItem(LEDGER_BUDGET_KEY) || "{}"
  );
  const [transactions, setTransactionList] = useState(localTransactions);
  const [budget, setBudget] = useState(localBudget);
  const [totalCredit, setTotalCredit] = useState(0);
  const [totalDebit, setTotalDebit] = useState(0);

  useEffect(() => {
    if (transactions && transactions instanceof Array) {
      localStorage.setItem(
        LEDGER_TRANSACTION_KEY,
        JSON.stringify(transactions)
      );
      const { credit, debit } = groupByTransactionType(transactions, wallet);
      setTotalCredit(credit);
      setTotalDebit(debit);
    }
  }, [LEDGER_TRANSACTION_KEY, transactions, wallet]);

  const createTransaction = (newTransaction) => {
    setTransactionList((oldTransactions) => [
      ...oldTransactions,
      newTransaction,
    ]);
  };

  const expensesListFull = transactions?.filter(
    (item) => item.type === "debit" && item.purposeId === wallet
  );

  const expensesList = expensesListFull
    ?.reverse()
    .map(({ categoryId, date, description, value }) => ({
      title: CATEGORY[categoryId],
      desc: description,
      value,
      date: date ? date.slice(0, 10) : "",
    }));

  const expensesByCategory = expensesListFull?.reduce((cur, expense) => {
    const curExpense = cur[expense.categoryId];
    if (curExpense) {
      cur[expense.categoryId] = {
        ...curExpense,
        value: curExpense.value + expense.value,
      };
    } else {
      cur[expense.categoryId] = {
        category: CATEGORY[expense.categoryId],
        value: expense.value,
        categoryId: expense.categoryId,
      };
    }
    return cur;
  }, {});

  useEffect(() => {
    if (budget && Object.keys(budget).length)
      localStorage.setItem(LEDGER_BUDGET_KEY, JSON.stringify(budget));
  }, [LEDGER_BUDGET_KEY, budget]);

  const setWalletBudget = (newVal) => {
    setBudget({
      ...budget,
      [wallet]: newVal,
    });
  };

  return {
    expensesList,
    transactions: transactions.filter((tx) => tx.purposeId === wallet),
    createTransaction,
    accounting: {
      funds: Number(totalCredit - totalDebit),
      expense: Number(totalDebit),
      budget: budget?.[wallet] ?? DEFAULT_BUDGET[wallet],
    },
    expensesByCategory,
    setWalletBudget,
  };
};

export default useTransaction;
