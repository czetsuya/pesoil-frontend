import BaseService from './BaseService';

import ApplicationStore from '../redux/ApplicationStore'

class Ledgers extends BaseService {

	list() {
		const state = ApplicationStore.getState();
		const { authContext } = state;
		return this.serviceConnector().callApi({ url: `/ledgers/${authContext.externalReferenceId}` });
	}

	retrieve(ledgerId) {
		return this.serviceConnector().callApi({ url: `/ledgers/${ledgerId}` });
	}

	create(userAccount) {
		return this.serviceConnector().invokeRequest({
			url: '/ledgers',
			method: 'POST',
			data: userAccount,
		});
    }
}

const ledgers = new Ledgers();

export default ledgers;