export const HOME_PATH = '/home';
export const ABOUT_PATH = '/about';
export const DASHBOARD_PATH = '/';
export const LEDGER_PATH = '/ledger';
export const NEW_LEDGER_PATH = '/new';
export const HARVEST_PATH = '/harvest';
