export const COLORS = {
    themeGreen: '#CEFA76',
    themeRed: '#FF6E6E',
    themeTeal: '#6ECFD8',
    themeDark: '#414141',
    themeOrange: '#F7B82D',
    themeBlue: '#5271FF',
    themeGray: '#C4C4C4',
    themeGreenDark: '#ABD752',
    themePurple: "#5271FF",
}

export const CATEGORY_COLOR = {
    1: COLORS.themeOrange,
    2: COLORS.themeGreenDark,
    3: COLORS.themeTeal,
    4: COLORS.themeGray,
    5: COLORS.themeOrange,
    6: COLORS.themeGreenDark,
    7: COLORS.themeTeal,
    8: COLORS.themeBlue,
    9: COLORS.themePurple,
    10: COLORS.themeGreen,
    11: COLORS.themeDark,
    12: COLORS.themeRed,
    13: COLORS.themeGray,
}