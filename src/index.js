import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from './serviceWorker';
import AppWrapper from './AppWrapper';

import Amplify, { Auth, Hub, API } from 'aws-amplify';
import awsconfig from "./aws-config/awsconfig-prod.json";
import awsauth from "./aws-config/awsauth-prod.json";

Amplify.configure(awsconfig)
Auth.configure({ oauth: awsauth })

ReactDOM.render(<AppWrapper />, document.getElementById("root"));
serviceWorker.register();
