import { withStyles } from "@material-ui/core/styles";

import LinearProgress from "@material-ui/core/LinearProgress";
import { COLORS } from '../constants/colors';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: COLORS.themeRed,
  },
  bar: {
    borderRadius: 5,
    backgroundColor: COLORS.themeGreenDark,
  },
}))(LinearProgress);

export const BudgetLinearProgress = withStyles(() => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: COLORS.themeGray,
  },
  bar: {
    borderRadius: 5,
    backgroundColor: COLORS.themeGreenDark,
  },
}))(LinearProgress);

export const AllocationProgress = (secondary = COLORS.themeGreenDark) => withStyles((props) => {
  return ({
    root: {
      height: 40,
      borderRadius: 5,
    },
    colorPrimary: {
      backgroundColor: COLORS.themeGray,
    },
    bar: {
      borderRadius: 5,
      backgroundColor: secondary,
    },
  })
})(LinearProgress);

export default BorderLinearProgress;
