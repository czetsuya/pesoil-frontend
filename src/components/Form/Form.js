import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import notebook from "../../assets/notebook.svg";
import Grid from "@material-ui/core/Grid";
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundImage: `url(${notebook})`,
    backgroundSize: 'cover',
    paddingLeft: '80px',
  },
  papernobg: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    padding: '20px 30px',
    overflowY: 'scroll',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  formBG: {
    height: '450px',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    fontSize: '20px',
  },
  title: {
    textAlign: 'center',
    flex: 1,
    margin: "40px 0"
  },
  titleBG: {
    margin: "40px 0 20px 0"
  }
}));

export default function Form({ title, children, handleSubmit, submitLabel = "Submit", hasBG }) {
  let history = useHistory()
  const classes = useStyles();
  const handleClick = (e) => {
    e.preventDefault();
    handleSubmit();
  }

  const handleCancelClick = (e) => {
    e.preventDefault();
    history.goBack();
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Typography
        component="h1"
        variant="h5"
        className={`${classes.title} ${hasBG ? "" : classes.titleBG}`}
      >
        {title}
      </Typography>
      <div className={hasBG ? classes.paper : classes.noBG}>
        <form
          className={`${classes.form} ${hasBG ? classes.formBG : ""}`}
          noValidate
        >
          {children}
          <Grid container className={classes.root} spacing={2}>
            <Grid item md={6}>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="secondary"
                className={classes.submit}
                onClick={handleCancelClick}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item md={6}>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="secondary"
                className={classes.submit}
                onClick={handleClick}
              >
                {submitLabel}
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
