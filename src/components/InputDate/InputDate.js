import React from 'react';
import TextField from '@material-ui/core/TextField';


export default function InputDate({
    label,
    name,
    value,
    handleChange,
}) {
  return (
      <TextField
        id={name}
        label={label}
        variant="outlined"
        color="secondary"
        type="date"
        margin="normal"
        fullWidth
        InputLabelProps={{
          shrink: true,
        }}
        value={value}
        onChange={(e) => handleChange(e.target.value)}
      />
  );
}