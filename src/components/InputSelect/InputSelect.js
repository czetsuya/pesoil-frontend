import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: '100%',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function SimpleSelect({
  value,
  handleChange,
  label,
  options,
  name,
  required = false,
}) {
  const classes = useStyles();
  const renderOptions = (item) => (
    <MenuItem key={item.value} value={item.value}>{item.label}</MenuItem>
  );
  return (
    <FormControl variant="outlined" color="secondary" className={classes.formControl}>
        <InputLabel id={name}>{label}</InputLabel>
        <Select
          name={name}
          labelId={name}
          label={label}
          value={value}
          defaultValue={value}
          onChange={(e) => handleChange(e.target.value)}
          required={required}
          fullwidth="true"
        >
          {options.map(renderOptions)}
        </Select>
      </FormControl>
  );
}
