import React from "react";
import { Doughnut } from "react-chartjs-2";

const DoughnutChart = ({ data, title }) => {
  return (
    <div>
      <h2 style={{ textAlign: 'center', marginBottom: '12px' }}>{title}</h2>
      <Doughnut data={data} />
    </div>
  );
};

export default DoughnutChart;
