import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Grid from "@material-ui/core/Grid";
import { COLORS } from '../constants/colors';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: '100%',
  },
  demo: {
    color: COLORS.themeDark,
  },
  item: {
    borderBottom: '1px solid #FFF',
    margin: "5px 0",
    borderRadius: "5px",
  },
}));

const ListItems = ({ title, items, bg = COLORS.themeTeal, isNegative }) => {
  const classes = useStyles();
  return (
    <Grid item xs={12} md={12} sm={12}>
      <h2 style={{ textAlign: "center", marginBottom: "20px", marginTop: '40px' }}>
        {title}
      </h2>
      <div className={classes.demo}>
        <List style={{ paddingTop: 0 }}>
          {items?.map(({ title, date, value }, i) =>(
            <ListItem key={`expense-${i}`} className={classes.item} style={{ backgroundColor: bg }}>
              <ListItemText
                primary={title}
                secondary={date ?? null}
              />
              {isNegative? '-' : ''}₱{value}
            </ListItem>
          ))}
        </List>
      </div>
    </Grid>
  );
};

export default ListItems;
