import React from "react";
import { useHistory, useLocation } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import * as PATHS from "../../constants/paths";
import { COLORS } from "../../constants/colors";
import HomeIcon from "../../assets/home.svg";
import BookIcon from "../../assets/books.svg";

import { useAuthContext } from "../../hooks/useAuthenticationContext";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  bar: {
    backgroundColor: COLORS.themeGreen,
  },
}));

const TopBar = () => {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { auth } = useAuthContext();

  const ledgerId = localStorage.activeLedger;
  const backToNoteBook = location.pathname.includes("add");
  const onNoteBook = location.pathname.includes(
    `${PATHS.LEDGER_PATH}/${ledgerId}`
  );
  const onHome = location.pathname === "/";
  const handleClick = (event) => {
    if (onHome) {
      history.push(PATHS.HOME_PATH);
    } else if (backToNoteBook && ledgerId) {
      history.push(`${PATHS.LEDGER_PATH}/${ledgerId}`);
    } else {
      history.push(PATHS.DASHBOARD_PATH);
    }
  };

  const handleClose = (nextPath) => {
    setAnchorEl(null);
    history.push(nextPath);
  };

  const handleRecord = () => history.push(`${PATHS.HARVEST_PATH}/${localStorage.activeLedger}`);
  const loggedIn = auth.isAuthenticated;

  return (
    <AppBar position="static" stlye={classes.bar}>
      <Toolbar>
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <img
            src={backToNoteBook ? BookIcon : HomeIcon}
            style={{ maxWidth: 30 }}
            alt="Pesoil Home"
          />
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        />
        <Typography variant="h6" className={classes.title}>
          PESOIL
        </Typography>
        {onNoteBook ? (
          <Button
            style={{ fontSize: "18px" }}
            color="inherit"
            onClick={loggedIn ? () => handleRecord() : () => auth.signIn()}
          >
            {loggedIn ? "Record Harvest" : "Login"}
          </Button>
        ) : (<Button
          style={{ fontSize: "18px" }}
          color="inherit"
          onClick={loggedIn ? () => auth.signOut() : () => auth.signIn()}
        >
          {loggedIn ? "Logout" : "Login"}
        </Button>)}
      </Toolbar>
    </AppBar>
  );
};

export default TopBar;
