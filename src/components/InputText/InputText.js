import React from "react";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";

export default function InputText({
  name,
  required = false,
  type = "text",
  label,
  autoComplete = undefined,
  id = undefined,
  value,
  handleChange,
  autoFocus = false,
  max = undefined,
  disabled = false,
  adornment = undefined,
  endAdornment = undefined,
}) {
  return (
    <TextField
      variant="outlined"
      color="secondary"
      margin="normal"
      required={required}
      fullWidth
      disabled={disabled}
      id={id ?? name}
      type={type}
      label={label}
      name={name}
      autoComplete={autoComplete}
      autoFocus={autoFocus}
      value={value}
      onChange={(e) => handleChange(e.target.value)}
      InputProps={{
        inputProps: { min: 0, max },
        startAdornment: adornment ? (
          <InputAdornment position="start">{adornment}</InputAdornment>
        ) : undefined,
        endAdornment: endAdornment ? (
          <InputAdornment position="end">{endAdornment}</InputAdornment>
        ) : undefined,
      }}
    />
  );
}
