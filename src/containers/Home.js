import React, { useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom";
import qs from "qs";
import * as PATHS from "../constants/paths";
import AppLogo from '../assets/logo.svg'
//import { loginURL } from "../auth";
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';

const Home = () => {
  
  const location = useLocation();
  const history = useHistory();
  const result = qs.parse(location.search, { ignoreQueryPrefix: true });
  const code = result?.code;
  useEffect(() => {
    const getUser = async (accessCode) => {
      try {
        localStorage.setItem('token', '123');
        history.push(PATHS.DASHBOARD_PATH);
        // const creds = await awsCallTokenEndpoint(accessCode);
        // if (creds?.access_token) {
        //   localStorage.setItem("token", creds.access_token);
        //   history.push(PATHS.DASHBOARD_PATH);
        // }
      } catch (error) {
        console.error(error);
      }
    };
    if (code) {
      getUser(code);
    }
  }, [code, history]);

  return (
    <>
      <Grid container style={{ padding: '20px', textAlign: 'center' }}>
        <Grid item xs={12}>
          <img src={AppLogo} style={{ maxWidth: '100%', padding: '10px 0'}} alt="Pasoil Logo" />
          <h2>Welcome to Pesoil!</h2>
          <p>
            Farming is a passion, lifestyle, hobby - and livelihood. Pesoil helps manage your finances and teaches you how to make the most out of your farm.
          </p>
        </Grid>
      </Grid>
      <Grid container spacing={1} style={{ textAlign: 'center' }}>
        <Grid item xs={6}>
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            style={{ fontSize: '20px' }}
            onClick={() => history.push(PATHS.DASHBOARD_PATH)}
          >
            Get Started
          </Button>
        </Grid>
        <Grid item xs={6}>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="secondary"
          style={{ fontSize: '20px' }}
          onClick={() => window.open('https://www.youtube.com/channel/UCkAb7BLw08HZY3q7LCL6vpg', '_blank')}
        >
            Watch Guide
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default Home;
