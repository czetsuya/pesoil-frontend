import React, { useEffect } from "react";
import qs from "qs";
import { useHistory, useLocation } from "react-router-dom";
import AuthWrapper from "./AuthWrapper";
import useLedger from "../hooks/useLedger";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import * as PATHS from "../constants/paths";
import { COLORS } from "../constants/colors";
import cornNote from '../assets/cornNote.svg';
import riceNote from '../assets/rice.svg';
import sugarCane from '../assets/sugarCane.svg';
import sweetPotato from '../assets/sweetPotato.svg';

const useStyles = makeStyles((theme) => ({
  submit: {
    margin: theme.spacing(3, 0, 2),
    fontSize: '20px',
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(1),
      padding: theme.spacing(1),
      width: "100%",
    },
  },
  ledgerItem: {
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    backgroundColor: COLORS.themeGray,
  },
  ledgerImage: {
    maxWidth: "75px",
    padding: "10px",
  }
}));

const CROPS_SVG = {
  1: riceNote,
  2: cornNote,
  3: sugarCane,
  4: sweetPotato,
}

const Dashboard = () => {

  const { ledgerList } = useLedger();
  const classes = useStyles();
  const history = useHistory();

  const getSVG = (name) => CROPS_SVG[name] || riceNote;

  const handleClick = (ledgerId) => {
    history.push(`${PATHS.LEDGER_PATH}/${ledgerId}`);
    localStorage.setItem('activeLedger', ledgerId);
  }

  const location = useLocation();
  const searchParams = qs.parse(location.search, { ignoreQueryPrefix: true })
  
  useEffect(() => {
    if (searchParams?.dev === "clear") {
      localStorage.clear();
      window.location.reload(true);
      history.push(PATHS.HOME_PATH);
    }
    if (searchParams?.ai === "reset") {
      localStorage.shownAI = "[]";
    }
    if (searchParams?.demo === "true") {
      localStorage.shownAI = "[]";
      localStorage.setItem('pesoil:ledgers', '[{"id":"ylt9x","name":"Cycle 1","cropType":1,"landArea":"1000","startDate":"2019-10-01"},{"id":"hky3k","name":"Cycle 2","cropType":2,"landArea":"1000","startDate":"2019-10-05","endDate":"2019-10-06","harvestUnit":2,"harvestAmount":"200","harvestSales":"50","expenses":31,"loans":10,"profit":9},{"id":"z617g","name":"Cycle 3","cropType":3,"landArea":"01000","startDate":"2019-10-09"}]');
      localStorage.setItem('pesoil:budget:hky3k', '{"2":{"5":20,"6":20,"7":20,"8":5,"9":5,"10":5,"11":5,"12":10,"13":10}}');
      localStorage.setItem('pesoil:budget:ylt9x', '{"2":{"5":100,"6":100,"7":100,"8":100,"9":100,"10":100,"11":100,"12":100,"13":100}}');
      localStorage.setItem('pesoil:budget:z617g', '{"2":{"5":5000,"6":5000,"7":5000,"8":2000,"9":2500,"10":3000,"11":1000,"12":5000,"13":5000}}');
      localStorage.setItem('pesoil:transactions:ylt9x', '[{"id":"hlg4s","description":"Cycle 1","date":"2020-08-25T02:01:59.936Z","value":1000,"purposeId":2,"categoryId":"","isFromLoan":true,"ledgerId":"ylt9x","type":"credit"},{"id":"qi6b8","description":"Fertilizer","date":"2020-08-25T02:04:36.216Z","value":50,"purposeId":2,"categoryId":12,"isFromLoan":false,"ledgerId":"ylt9x","type":"debit"},{"id":"e6ha8","description":"Tools","date":"2020-08-25T02:05:12.136Z","value":5010,"purposeId":2,"categoryId":10,"isFromLoan":false,"ledgerId":"ylt9x","type":"debit"}]');
      localStorage.setItem('pesoil:transactions:hky3k', '[{"id":"c22so","description":"Capital","date":"2020-08-24T15:24:22.223Z","value":100,"purposeId":2,"categoryId":"","isFromLoan":false,"ledgerId":"hky3k","type":"credit"},{"id":"mai7u","description":"1","date":"2020-08-24T15:24:55.150Z","value":10,"purposeId":2,"categoryId":5,"isFromLoan":false,"ledgerId":"hky3k","type":"debit"},{"id":"v4jqn","description":"s","date":"2020-08-24T15:25:08.574Z","value":3,"purposeId":2,"categoryId":11,"isFromLoan":false,"ledgerId":"hky3k","type":"debit"},{"id":"watj8","description":"T","date":"2020-08-24T15:25:20.538Z","value":5,"purposeId":2,"categoryId":10,"isFromLoan":false,"ledgerId":"hky3k","type":"debit"},{"id":"8i56u","description":"L","date":"2020-08-24T15:25:36.402Z","value":4,"purposeId":2,"categoryId":9,"isFromLoan":false,"ledgerId":"hky3k","type":"debit"},{"id":"ehe6x","description":"F","date":"2020-08-24T15:25:50.028Z","value":9,"purposeId":2,"categoryId":12,"isFromLoan":false,"ledgerId":"hky3k","type":"debit"},{"id":"77awu","description":"LOAD","date":"2020-08-24T15:26:10.328Z","value":20,"purposeId":2,"categoryId":"","isFromLoan":true,"ledgerId":"hky3k","type":"credit"},{"id":"kn8a6","description":"PA","date":"2020-08-24T15:26:29.676Z","value":10,"purposeId":2,"categoryId":99,"isFromLoan":true,"ledgerId":"hky3k","type":"debit"}]');
      localStorage.setItem('pesoil:transactions:z617g', '[{"id":"30h07","description":"Capital","date":"2020-08-24T14:59:17.469Z","value":100000,"purposeId":2,"categoryId":"","isFromLoan":false,"ledgerId":"z617g","type":"credit"},{"id":"hpdxt","description":"Pests","date":"2020-08-24T15:00:06.387Z","value":2500,"purposeId":2,"categoryId":5,"isFromLoan":false,"ledgerId":"z617g","type":"debit"},{"id":"x574s","description":"Weeds","date":"2020-08-24T15:00:21.366Z","value":1450,"purposeId":2,"categoryId":6,"isFromLoan":false,"ledgerId":"z617g","type":"debit"},{"id":"ishuu","description":"Insects","date":"2020-08-24T15:00:32.876Z","value":3090,"purposeId":2,"categoryId":7,"isFromLoan":false,"ledgerId":"z617g","type":"debit"},{"id":"zu048","description":"Clean","date":"2020-08-24T15:05:54.224Z","value":900,"purposeId":2,"categoryId":8,"isFromLoan":false,"ledgerId":"z617g","type":"debit"},{"id":"yhp7t","description":"Work","date":"2020-08-24T15:06:06.372Z","value":760,"purposeId":2,"categoryId":9,"isFromLoan":false,"ledgerId":"z617g","type":"debit"},{"id":"x5kxr","description":"Plow","date":"2020-08-24T15:06:20.470Z","value":4500,"purposeId":2,"categoryId":10,"isFromLoan":false,"ledgerId":"z617g","type":"debit"},{"id":"o1624","description":"190","date":"2020-08-24T15:06:38.182Z","value":190,"purposeId":2,"categoryId":11,"isFromLoan":false,"ledgerId":"z617g","type":"debit"},{"id":"r06c1","description":"4000","date":"2020-08-24T15:06:52.572Z","value":4000,"purposeId":2,"categoryId":12,"isFromLoan":false,"ledgerId":"z617g","type":"debit"}]');
      history.push(PATHS.HOME_PATH);
      window.location.reload();
    }
  }, [history, location, searchParams]);
  const renderLedger = (ledgerItem) => {
    return (
      <Paper
        elevation={3}
        key={ledgerItem.id}
        className={classes.ledgerItem}
        onClick={() => handleClick(ledgerItem.id)}
      >
        <img className={classes.ledgerImage} src={getSVG(ledgerItem.cropType)} alt="corn note" />
        <div>
          <Typography variant="h5">{ledgerItem.name}</Typography>
          <span style={{ fontSize: '20px '}}>{ledgerItem.startDate}</span>
        </div>
      </Paper>
    );
  };
  const handleCreateLedger = () => history.push(PATHS.NEW_LEDGER_PATH);
  const user = localStorage.user ? JSON.parse((localStorage)) : null;
  const name = user?.attributes?.given_name;
  const nameString = name ? ` ${name}` : '';
  return (
    <AuthWrapper>
      <h2>
       {`Good morning${nameString}!`}
        <br />
        {ledgerList.length
          ? ""
          : "Create a notebook to get started."}
      </h2>
      <div className={classes.container}>{ledgerList.map(renderLedger)}</div>
      <Button
        fullWidth
        variant="contained"
        color="secondary"
        className={classes.submit}
        onClick={handleCreateLedger}
      >
        Create New Notebook
      </Button>
    </AuthWrapper>
  );
};

export default Dashboard;
