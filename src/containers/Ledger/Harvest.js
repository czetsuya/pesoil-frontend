import { connect } from 'react-redux';
import React, { useState, useEffect } from "react";
import useLedger from "../../hooks/useLedger";
import useTransaction from "../../hooks/useTransaction";
import AuthWrapper from "../AuthWrapper";
import Form from "../../components/Form/Form";
import InputText from "../../components/InputText/InputText";
import InputSelect from "../../components/InputSelect/InputSelect";
import InputDate from "../../components/InputDate/InputDate";
import * as PATHS from "../../constants/paths";
import * as LedgerActions from '../../redux/modules/Ledgers';

const processTransactions = (farmTx) => {
  const creditWalletItems = [];
  var debitWalletItem = null;
  const walletOperations = [];

  var i;
  const ledgerWallet = {};
  ledgerWallet.walletId = 2;
  ledgerWallet.walletItems = [];
  for (i = 0; i < farmTx.length; i++) {
    if (farmTx[i].type === "credit") {
      const walletItem = {};
      walletItem.description = farmTx[i].description;
      walletItem.createdAt = farmTx[i].date;
      walletItem.amount = farmTx[i].value;
      walletItem.isLoan = farmTx[i].isFromLoan;
      creditWalletItems.push(walletItem);

    } else if (farmTx[i].type === "debit") {
      if (debitWalletItem == null) {
        debitWalletItem = {};
        debitWalletItem.description = farmTx[i].description;
        debitWalletItem.created = farmTx[i].date;
        debitWalletItem.isLoan = farmTx[i].isFromLoan;
      }
      const walletOperation = {};
      walletOperation.amount = farmTx[i].value;
      walletOperation.createdAt = farmTx[i].date;
      walletOperations.push(walletOperation);
    }
  }

  ledgerWallet.walletItems = creditWalletItems;
  if (debitWalletItem != null) {
    debitWalletItem.walletOperations = walletOperations;
    ledgerWallet.walletItems.push(debitWalletItem);
  }

  return ledgerWallet;
}

const translateStorageToJson = (ledger, personalTx, farmTx, user) => {
  const userAccount = {};

  userAccount.externalReferenceId = user.externalReferenceId;
  userAccount.firstName = user.firstName;
  userAccount.lastname = user.lastName;

  ledger.code = ledger.id;
  ledger.harvestUnitId = ledger.harvestUnit;

  delete ledger.harvestUnit;
  delete ledger.id;

  ledger.ledgerWallets = [];
  ledger.ledgerWallets.push(processTransactions(personalTx));
  ledger.ledgerWallets.push(processTransactions(farmTx));

  userAccount.ledgers = [];
  userAccount.ledgers.push(ledger);

  return userAccount;
}

const NewHarvest = ({ dispatch, authContext, match: { params }, history, location: { state } }) => {
  const { endLedger, ledgerList } = useLedger();
  const { transactions } = useTransaction(params.id, 2);
  const [endDate, setEndDate] = useState("");
  const [harvestUnit, setHarvestUnit] = useState("");
  const [harvestSales, setHarvestSales] = useState("");
  const [harvestAmount, setHarvestAmount] = useState("");
  const [created, setCreated] = useState(false);
  const { getActiveLedger } = useLedger();
  const useTx = useTransaction(params.id, 1);

  const handleRecordHarvest = () => {
    if (endDate && harvestUnit && harvestAmount && harvestSales) {
      const accounting = transactions.reduce(
        (acc, cur) => {
          if (cur.type === "credit" && cur.isFromLoan) {
            acc.loans += cur.value;
          }
          if (cur.type === "debit") {
            if (cur.isFromLoan) {
              acc.loans -= cur.value;
            } else {
              acc.expenses += cur.value;
            }
          }
          return acc;
        },
        {
          expenses: 0,
          loans: 0,
        }
      );
      endLedger(params.id, {
        endDate,
        harvestSales,
        harvestUnit,
        harvestAmount,
        expenses: accounting.expenses,
        loans: accounting.loans,
      });
      setCreated(true);

      const newLedger = getActiveLedger(params.id);
      newLedger.harvestSales = harvestSales;
      newLedger.harvestAmount = harvestAmount;
      newLedger.harvestUnitId = harvestUnit;
      newLedger.endDate = endDate;
      newLedger.expenses = accounting.expenses;
      newLedger.loans = accounting.loans;
      const ua = translateStorageToJson(newLedger, useTx.transactions, transactions, authContext);
      dispatch(LedgerActions.create(ua))
    }
  };

  useEffect(() => {
    const ledger = ledgerList?.find((ledger) => ledger.id === params.id);
    if (created && ledger && !!ledger.endDate) {
      created && history.push(`${PATHS.LEDGER_PATH}/${params.id}`);
    }
  }, [created, history, ledgerList, params.id]);
  return (
    <AuthWrapper>
      <Form title="Record Harvest" handleSubmit={handleRecordHarvest} hasBG>
        <InputText
          required
          name="harvestSales"
          label="TOTAL HARVEST SALES"
          value={harvestSales}
          handleChange={(val) => setHarvestSales(val)}
          adornment="₱"
        />
        <InputSelect
          required
          name="harvestUnit"
          label="HARVEST UNIT"
          value={harvestUnit}
          handleChange={(val) => {
            setHarvestUnit(val);
          }}
          options={[
            { label: "Sacks", value: 1 },
            { label: "Kilogram", value: 2 },
          ]}
        />
        <InputText
          required
          name="harvestAmount"
          label="TOTAL AMOUNT"
          value={harvestAmount}
          handleChange={(val) => setHarvestAmount(val)}
          endAdornment={harvestUnit === 2 ? "KG" : "Sacks"}
        />
        <InputDate
          name="endDate"
          label="END DATE"
          value={endDate}
          handleChange={(val) => setEndDate(val)}
        />
      </Form>
    </AuthWrapper>
  );
};

export default connect(state => ({
  authContext: state.authContext,
}))(NewHarvest);