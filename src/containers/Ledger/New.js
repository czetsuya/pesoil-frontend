import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import AuthWrapper from "../AuthWrapper";
import useLedger from "../../hooks/useLedger";
import Form from "../../components/Form/Form";
import InputText from "../../components/InputText/InputText";
import InputSelect from "../../components/InputSelect/InputSelect";
import InputDate from "../../components/InputDate/InputDate";
import * as PATHS from "../../constants/paths";

const NewLedger = () => {
  const history = useHistory();
  const { ledgerList, createLedger } = useLedger();
  const [name, setName] = useState("");
  const [cropType, setCropType] = useState("");
  const [startDate, setStartDate] = useState("");
  const [landArea, setLandArea] = useState();
  const [created, setCreated] = useState(false);
  
  const handleCreateLedger = () => {
    if (name && startDate && cropType) {
      createLedger({
        id: Math.random().toString(36).substr(2, 5),
        name,
        cropType,
        landArea,
        startDate,
      });
      setCreated(true)
    }
  };

  useEffect(() => {
    created && history.push(PATHS.DASHBOARD_PATH);
  }, [created, history, ledgerList.length]);

  return (
    <AuthWrapper>
      <Form title="Create a new notebook" handleSubmit={handleCreateLedger} hasBG>
        <InputText
          required
          name="ledgerName"
          label="NOTEBOOK NAME"
          value={name}
          autoFocus
          handleChange={(val) => setName(val)}
        />
        <InputSelect
          required
          name="cropType"
          label="CROP TYPE"
          value={cropType}
          handleChange={(val) => {
            setCropType(val);
          }}
          options={[
            { label: "Rice", value: 1 },
            { label: "Corn", value: 2 },
            { label: "Sugar Cane", value: 3 },
            { label: "Sweet Potato", value: 4},
          ]}
        />
        <InputText
          required
          name="landArea"
          label="LAND AREA(SQM)"
          value={landArea}
          type="number"
          handleChange={(val) => setLandArea(val)}
        />
        <InputDate
          name="startDate"
          label="START DATE"
          value={startDate}
          handleChange={(val) => setStartDate(val)}
        />
      </Form>
    </AuthWrapper>
  );
};

export default NewLedger;
