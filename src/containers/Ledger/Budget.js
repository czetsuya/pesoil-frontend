import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import AuthWrapper from "../AuthWrapper";
import useTransaction from "../../hooks/useTransaction";
import Form from "../../components/Form/Form";
import InputText from "../../components/InputText/InputText";
import * as PATHS from "../../constants/paths";
import { CATEGORY } from "../../hooks/useTransaction";
import { BudgetLinearProgress } from "../../components/Progress";

const NewBudget = ({ match: { params }, history, location: { state } }) => {
  const ledgerId = params.id;
  const purpose = state?.wallet || 2;
  const [created, setCreated] = useState(false);
  const { accounting, setWalletBudget } = useTransaction(
    ledgerId,
    purpose || ""
  );
  const [xbudget, setXbudget] = useState(accounting.budget);
  const [sumAlloc, setSumAlloc] = useState(0);
  const [unalloc, setUnalloc] = useState(0);

  const handleCreateBudget = () => {
    setWalletBudget(xbudget);
    setCreated(true);
  };

  useEffect(() => {
    created &&
      history.push(`${PATHS.LEDGER_PATH}/${ledgerId}`, { wallet: purpose });
  }, [created, history, ledgerId, purpose]);

  const handleSlide = (key, val, cv) => {
    const x = unalloc + cv;
    setXbudget((oldXbudget) => ({
      ...oldXbudget,
      [Number(key)]: val > x ? oldXbudget[key] : val,
    }));
  };

  useEffect(() => {
    const sumAllocated = Object.values(xbudget ?? {}).reduce(
      (a, b) => a + b,
      0
    );
    setSumAlloc(sumAllocated);
    const unallocated = accounting?.funds - sumAllocated || 0;
    setUnalloc(unallocated);
  }, [xbudget, accounting.funds]);

  useEffect(() => {
    setXbudget(accounting.budget);
  }, [accounting.budget]);

  const renderBudgeters = () => {
    let items = [];
    items.push(
      <div style={{ display: "block" }}>
        <Grid
          key="bgtr-x"
          container
          spacing={2}
          style={{ alignItems: "center" }}
        >
          <Grid item xs={12}>
            <InputText
              label="Unallocated"
              required
              value={unalloc}
              type="number"
              max={unalloc}
              disabled
              adornment="₱"
            />
          </Grid>
        </Grid>
        <div>
          <BudgetLinearProgress
            variant="determinate"
            value={(100 * unalloc) / (sumAlloc + unalloc)}
          />
        </div>
      </div>
    );
    const newItems = Object.keys(xbudget ?? {}).map((catKey, index) => {
      const catVal = xbudget?.[catKey] || 0;
      return (
        <div style={{ display: "block" }}>
          <Grid
            container
            key={`bgtr-${index}`}
            spacing={2}
            style={{ alignItems: "center" }}
          >
            <Grid item xs={12}>
              <InputText
                required
                value={catVal || ""}
                label={CATEGORY[catKey]}
                type="number"
                max={unalloc}
                adornment="₱"
                handleChange={(val) => handleSlide(catKey, Number(val), catVal)}
              />
            </Grid>
          </Grid>
          <div>
            <BudgetLinearProgress
              variant="determinate"
              value={(100 * catVal) / (sumAlloc + unalloc)}
            />
          </div>
        </div>
      );
    });
    return [...items, ...newItems];
  };

  return (
    <AuthWrapper>
      <Form title="Allocate Budget" handleSubmit={handleCreateBudget}>
        {renderBudgeters()}
      </Form>
    </AuthWrapper>
  );
};

export default NewBudget;
