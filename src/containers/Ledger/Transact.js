import React, { useState, useEffect } from "react";
import AuthWrapper from "../AuthWrapper";
import useTransaction from "../../hooks/useTransaction";
import Form from "../../components/Form/Form";
import InputText from "../../components/InputText/InputText";
import InputSelect from "../../components/InputSelect/InputSelect";
import * as PATHS from "../../constants/paths";

const NewLedger = ({ match: { params }, history, location: { state } }) => {
  const ledgerId = params.id;
  const type = params.type;
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [source, setSource] = useState("");
  const [purpose, setPurpose] = useState(state?.wallet || "");
  const [value, setValue] = useState(0);
  const [created, setCreated] = useState(false);
  const {
    transactions,
    createTransaction,
    accounting,
    setWalletBudget,
  } = useTransaction(ledgerId, purpose || "");
  const [xbudget, setXbudget] = useState(accounting.budget);

  useEffect(() => {
    const allowedTypes = ["credit", "debit"];
    if (!allowedTypes.includes(type) || !ledgerId) {
      history.push(`${PATHS.LEDGER_PATH}/${ledgerId}`, { wallet: purpose });
    }
  }, [history, ledgerId, purpose, type]);

  const handleCreateTransaction = () => {
    if (description && value && purpose) {
      if (type === "debit" && !category) {
        return;
      }
      if (type === "credit" && !source) {
        return;
      }
      setWalletBudget(xbudget);
      createTransaction({
        id: Math.random().toString(36).substr(2, 5),
        description,
        date: new Date().toISOString(),
        value: Number(value),
        purposeId: purpose,
        categoryId: category,
        isFromLoan: type === "credit" ? (source === "loan") : category === 99,
        ledgerId,
        type,
      });
      setCreated(true);
    }
  };

  useEffect(() => {
    created &&
      history.push(`${PATHS.LEDGER_PATH}/${ledgerId}`, { wallet: purpose });
  }, [created, history, ledgerId, purpose, transactions.length]);

  useEffect(() => {
    setXbudget(accounting.budget);
  }, [accounting.budget]);

  return (
    <AuthWrapper>
      <Form
        title={`Add ${type === "credit" ? "Funds" : "Expense"}`}
        handleSubmit={handleCreateTransaction}
        hasBG
      >
        <InputSelect
          required
          name="purpose"
          label="Transaction Type"
          value={purpose}
          handleChange={(val) => {
            setPurpose(val);
          }}
          options={[
            { label: "Personal Wallet", value: 1 },
            { label: "Farm Wallet", value: 2 },
          ]}
        />
        <InputText
          required
          name="description"
          label="Description"
          value={description}
          handleChange={(val) => setDescription(val)}
        />
        {type === "debit" ? (
          <InputSelect
            required
            name="category"
            label="Category"
            value={category}
            handleChange={(val) => {
              setCategory(val);
            }}
            options={
              purpose === 1
                ? [
                    { label: "Food", value: 1 },
                    { label: "Utility / Bills", value: 2 },
                    { label: "School/Tuition", value: 3 },
                    { label: "Others", value: 4 },
                    { label: "Loan Payment", value: 99},
                  ]
                : [
                    { label: "Pesticide", value: 5 },
                    { label: "Herbicide", value: 6 },
                    { label: "Insecticide", value: 7 },
                    { label: "Maintenance / Cleaning", value: 8 },
                    { label: "Labor", value: 9 },
                    { label: "Renting of tools", value: 10 },
                    { label: "Seeds", value: 11 },
                    { label: "Fertilizers", value: 12 },
                    { label: "Others", value: 13 },
                    { label: "Loan Payment", value: 99},
                  ]
            }
          />
        ) : null}
        <InputText
          required
          name="value"
          label="Amount"
          value={value}
          type="number"
          handleChange={(val) => setValue(val)}
          adornment="₱"
        />
        {type === "credit" ? <InputSelect
          required
          name="source"
          label="Source"
          value={source}
          handleChange={(val) => {
            setSource(val);
          }}
          options={[
            {
              label: "Loan",
              value: "loan",
            },
            {
              label: "Personal" ,
              value: "personal",
            },
          ]}
        /> : null}
      </Form>
    </AuthWrapper>
  );
};

export default NewLedger;
