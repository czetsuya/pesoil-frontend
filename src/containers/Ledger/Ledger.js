import React, { useState, useEffect } from "react";
import AuthWrapper from "../AuthWrapper";
import useTransaction, { CATEGORY } from "../../hooks/useTransaction";
import useLedger from "../../hooks/useLedger";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Typography from "@material-ui/core/Typography";
import * as PATHS from "../../constants/paths";
import Doughnut from "../../components/Doughnut";
import ListItems from "../../components/List";
import Progress, { AllocationProgress } from "../../components/Progress";
import { COLORS, CATEGORY_COLOR } from "../../constants/colors";
import Modal from "@material-ui/core/Modal";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import useChatModal from "../../hooks/useChatModal";

const CROP = {
  1: "Rice",
  2: "Corn",
  3: "Sugar Cane",
  4: "Sweet Potato",
};

const UNIT = {
  1: "Sacks",
  2: "Kilogram",
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
    cursor: "pointer",
  },
  greenPaper: {
    backgroundColor: COLORS.themeGreenDark,
  },
  redPaper: {
    backgroundColor: COLORS.themeRed,
  },
  bluePaper: {
    backgroundColor: COLORS.themeBlue,
  },
  top: {
    padding: "10px 5px",
    alignItems: "center",
  },
  funds: {
    textAlign: "right",
    fontWeight: "bold",
  },
  actionButtons: {
    marginTop: "20px",
    alignItems: "center",
  },
  modalContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "&:focus": {
      outline: "unset",
    },
  },
  modal: {
    backgroundColor: "white",
    maxWidth: "360px",
    borderRadius: "5px",
    padding: "10px",
    "&:focus": {
      outline: "unset",
    },
  },
}));

const Ledger = ({ match: { params }, history, location: { state } }) => {
  const [wallet, setWallet] = useState(state?.wallet || 2);
  const [open, setOpen] = useState(false);
  const {
    transactions,
    accounting,
    expensesList,
    expensesByCategory,
  } = useTransaction(params.id, wallet);
  const { ledgerList } = useLedger();
  const classes = useStyles();
  const ledger = ledgerList.find((ledger) => ledger.id === params.id);
  const {
    setCocoParams,
    CocoModal,
    setOpen: setOpenCoco,
    setActiveContent,
  } = useChatModal();

  const handleAddFunds = () =>
    history.push(`${PATHS.LEDGER_PATH}/${params.id}/add/credit`, { wallet });
  const handleAddExpense = () =>
    history.push(`${PATHS.LEDGER_PATH}/${params.id}/add/debit`, { wallet });
  const handleBudget = () =>
    history.push(`${PATHS.LEDGER_PATH}/${params.id}/budget/add`, { wallet });
  const data = {
    labels: expensesByCategory
      ? Object.values(expensesByCategory).map((item) => item.category)
      : [],
    datasets: [
      {
        data: expensesByCategory
          ? Object.values(expensesByCategory).map((item) => item.value)
          : [],
        backgroundColor: [
          COLORS.themeOrange,
          COLORS.themeGreenDark,
          COLORS.themeTeal,
          COLORS.themeRed,
          COLORS.themeBlue,
          COLORS.themeDark,
          COLORS.themeGreen,
          COLORS.themePurple,
          COLORS.themeGray,
        ],
        hoverBackgroundColor: [
          COLORS.themeOrange,
          COLORS.themeGreenDark,
          COLORS.themeTeal,
          COLORS.themeRed,
          COLORS.themeBlue,
          COLORS.themeDark,
          COLORS.themeGreen,
          COLORS.themePurple,
          COLORS.themeGray,
        ],
      },
    ],
  };

  useEffect(() => {
    if (!!ledger.endDate) {
      setOpen(true);
    }
  }, [ledger.endDate, ledgerList, params.id]);

  const handleClose = () => {
    setOpen(false);
  };

  const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  const renderPair = (title, value, num) => {
    return (
      <Grid container>
        <Grid item xs={6}>
          <b>{title}</b>
        </Grid>
        <Grid item xs={6}>
          {num ? numberWithCommas(value) : value}
        </Grid>
      </Grid>
    );
  };

  const percentFunds =
    (accounting.funds * 100) / (accounting.expense + accounting.funds);

  const renderTop = () => (
    <Grid container spacing={1} className={classes.top}>
      <Grid item xs={6}>
        <FormControlLabel
          control={
            <Switch
              checked={wallet === 2}
              onChange={(e) => setWallet(e.target.checked ? 2 : 1)}
              name="wallet"
              color="secondary"
            />
          }
          label={wallet === 2 ? "Farm Wallet" : "Personal Wallet"}
        />
      </Grid>
      <Grid item xs={6}>
        <Typography className={classes.funds}>
          Funds: ₱{accounting.funds ?? 0}
        </Typography>
      </Grid>
    </Grid>
  );

  const renderBudgetAlloc = () => {
    const hasBudgetAlloc =
      accounting?.budget &&
      Object.values(accounting.budget).filter((z) => !!z).length;
    const budgetAlloc = Object.keys(accounting.budget).map((catId) => {
      const AllocationProgressComp = AllocationProgress(
        CATEGORY_COLOR[catId] || COLORS.themeGreenDark
      );
      const bdgAmount = accounting?.budget?.[catId];
      if (!bdgAmount) return null;
      const exp = expensesByCategory?.[catId] || {};
      const perc = (exp.value * 100) / bdgAmount;
      return (
        <Grid container style={{ margin: "10px 0" }} key={`rba-${catId}`}>
          <Grid item xs={12} style={{ position: "relative" }}>
            <AllocationProgressComp variant="determinate" value={perc} />
            <span style={{ position: "absolute", left: 13, top: 10 }}>
              {CATEGORY[catId]}
            </span>
            <span style={{ position: "absolute", right: 13, top: 10 }}>
              (₱{bdgAmount - (exp.value ?? 0)} / ₱{bdgAmount})
            </span>
          </Grid>
        </Grid>
      );
    });
    if (hasBudgetAlloc) {
      return (
        <>
          <h2
            style={{
              textAlign: "center",
              marginBottom: "20px",
              marginTop: "40px",
            }}
          >
            Budget Overview
          </h2>
          {budgetAlloc}
        </>
      );
    }
  };

  const equipExp = expensesByCategory?.[10]?.value || 0;
  const equipExpMax = 5000;
  const expPercent = (100 * (equipExp - equipExpMax)) / equipExpMax;
  useEffect(() => {
    const shownAI = localStorage.shownAI
      ? JSON.parse(localStorage.shownAI)
      : [];
    const show = !shownAI?.includes("EQSP");
    if (equipExp >= equipExpMax && !ledger.endDate && show) {
      setActiveContent("EQSP");
      setOpenCoco(true);
      setCocoParams([`${expPercent}%`, `₱${equipExpMax}`]);
      localStorage.shownAI = JSON.stringify([...shownAI, "EQSP"]);
    }
  }, [
    equipExp,
    expPercent,
    expensesByCategory,
    ledger.endDate,
    setActiveContent,
    setCocoParams,
    setOpenCoco,
  ]);

  const pestExp = expensesByCategory?.[7]?.value || 0;
  const pestExpMax = 2000;
  const pestExpPercent = (100 * (pestExp - pestExpMax)) / pestExpMax;

  useEffect(() => {
    const shownAI = localStorage.shownAI
      ? JSON.parse(localStorage.shownAI)
      : [];
    const show = !shownAI?.includes("WEATHER");
    if (pestExp >= pestExpMax && !ledger.endDate && show) {
      setActiveContent("WEATHER");
      setOpenCoco(true);
      setCocoParams([CROP[ledger.cropType] || "Rice"]);
      localStorage.shownAI = JSON.stringify([...shownAI, "WEATHER"]);
    }
  }, [pestExp, pestExpPercent, expensesByCategory, ledger.endDate, setActiveContent, setCocoParams, setOpenCoco, ledger.cropType]);

  useEffect(() => {
    const shownAI = localStorage.shownAI
      ? JSON.parse(localStorage.shownAI)
      : [];
    const show = !shownAI?.includes("PEST");
    if (show && accounting.funds > 110000 && !ledger.endDate) {
      setActiveContent("PEST");
      setOpenCoco(true);
      setCocoParams([CROP[ledger.cropType] || "Rice"]);
      localStorage.shownAI = JSON.stringify([...shownAI, "PEST"]);
    }
  }, [
    ledger.endDate,
    setActiveContent,
    setOpenCoco,
    accounting.funds,
    setCocoParams,
    ledger,
  ]);

  // useEffect(() => {
  //   //!ledger.endDate
  //   const shownAI = localStorage.shownAI ? JSON.parse(localStorage.shownAI): [];
  //   const show = !shownAI?.includes("EQSP");
  //   if (equipExp >= equipExpMax && (!!ledger.endDate && show)) {
  //     setActiveContent("EQSP");
  //     setOpenCoco(true);
  //     localStorage.shownAI = JSON.stringify([...shownAI, "EQSP"])
  //   }
  // }, [equipExp, expensesByCategory, ledger.endDate, setActiveContent, setOpenCoco])

  return (
    <AuthWrapper>
      <CocoModal activeId="EQSP" />
      {renderTop()}
      <Progress variant="determinate" value={percentFunds} />
      {/* <AllocationProgressComp variant="determinate" value={percentFunds} /> */}
      <h2 style={{ textAlign: "center" }}>
        {transactions.length ? null : "Add budget to get started"}
      </h2>
      <div className={classes.root}>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          className={classes.modalContainer}
        >
          <Grid container className={classes.modal}>
            <Grid item xs={12}>
              <h2 style={{ textAlign: "center" }}>Harvest Details</h2>
              <Divider variant="middle" />
              <Grid container style={{ padding: "20px" }}>
                <Grid item xs={12}>
                  {renderPair(
                    "Harvest:",
                    `${ledger?.harvestAmount} ${UNIT[ledger?.harvestUnit]} of ${
                      CROP[ledger?.cropType]
                    }`
                  )}
                  {renderPair("Sales:", `₱${ledger?.harvestSales}`, true)}
                  {renderPair("Expenses:", `₱${ledger?.expenses}`, true)}
                  {renderPair("Unpaid Loans:", `₱${ledger?.loans}`, true)}
                  {renderPair("Started:", ledger?.startDate)}
                  {renderPair("Ended:", ledger?.endDate)}
                  <h2>
                    <b>Total Profit:</b> ₱
                    {numberWithCommas(ledger?.profit || 0)}
                  </h2>
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="secondary"
                style={{ fontSize: "20px" }}
                onClick={handleClose}
              >
                Done
              </Button>
            </Grid>
          </Grid>
        </Modal>
        {transactions?.length ? (
          <Grid container spacing={1} direction="row-reverse">
            <Grid item md={3} sm={12}>
              {accounting.funds ? (
                <Grid container spacing={1} className={classes.actionButtons}>
                  <Grid item md={12} sm={4}>
                    <Paper
                      className={`${classes.paper} ${classes.greenPaper}`}
                      onClick={handleAddFunds}
                    >
                      + Funds
                    </Paper>
                  </Grid>
                  <Grid item md={12} sm={4}>
                    <Paper
                      className={`${classes.paper} ${classes.bluePaper}`}
                      onClick={handleBudget}
                    >
                      + Budget
                    </Paper>
                  </Grid>
                  <Grid item md={12} sm={4}>
                    <Paper
                      className={`${classes.paper} ${classes.redPaper}`}
                      onClick={handleAddExpense}
                    >
                      + Expense
                    </Paper>
                  </Grid>
                </Grid>
              ) : null}
            </Grid>
            <Grid item md={9} xs={12}>
              {expensesByCategory && Object.keys(expensesByCategory).length ? (
                <Doughnut data={data} title="Expenses Overview" />
              ) : null}
            </Grid>
          </Grid>
        ) : (
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper
                className={`${classes.paper} ${classes.greenPaper}`}
                onClick={handleAddFunds}
              >
                Add Budget
              </Paper>
            </Grid>
          </Grid>
        )}
      </div>
      {renderBudgetAlloc()}
      <Grid item xs={12} md={12} sm={12}>
        {accounting.expense && expensesList ? (
          <ListItems
            isNegative
            title="Recent Expenses"
            items={expensesList?.splice(0, 3)}
          />
        ) : null}
      </Grid>
    </AuthWrapper>
  );
};

export default Ledger;
