import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import * as PATHS from '../constants/paths';

const AuthWrapper = ({ children }) => {
    const history = useHistory();
    useEffect(() => {
      // Show the homepage atleast once
      if (!localStorage.visited) {
          history.push(PATHS.HOME_PATH);
      }
      localStorage.setItem('visited', 'beta');
    }, [history])
    return <>{children}</>;
  };

export default AuthWrapper;
