import React from "react";
import { connect } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Container from "@material-ui/core/Container";
import { createMuiTheme } from '@material-ui/core/styles';

import TopBar from "./components/TopBar/TopBar";
import Dashboard from "./containers/Dashboard";
import Home from "./containers/Home";
import NewLedger from './containers/Ledger/New';
import Ledger from './containers/Ledger/Ledger';
import Transact from './containers/Ledger/Transact';
import Budget from './containers/Ledger/Budget';
import Harvest from './containers/Ledger/Harvest';
import * as PATHS from "./constants/paths";
import { ThemeProvider } from '@material-ui/styles';
import { COLORS } from './constants/colors';

import { AuthContext } from "./hooks/useAuthenticationContext";
import useAuthentication from "./hooks/useAuthentication";
import CircularUnderLoad from './components/ProgressLoader'

// Test API call
import * as LedgerActions from './redux/modules/Ledgers';
//


const theme = createMuiTheme({
  palette: {
    primary: {
      // Purple and green play nicely together.
      light: COLORS.themeGreen,
      main: COLORS.themeGreen,
      dark: COLORS.themeBlue,
    },
    secondary: {
      // This is green.A700 as hex.
      main: COLORS.themeTeal,
    },
  },
  typography: {
    fontFamily: [
      'Oswald',
      'sans-serif',
    ].join(','),
  },
});

const App = ( {dispatch} ) => {

  const auth = useAuthentication({dispatch});

  if(auth.isAuthenticated) {
    // console.log("ledgers: ", dispatch(LedgerActions.list()));
  }

  if(auth.isLoading) {
    return (<ThemeProvider theme={theme}><CircularUnderLoad/></ThemeProvider>)
  }
  return (
    <ThemeProvider theme={theme}>
      <AuthContext.Provider value={{ auth }}>
        <Router>
          <TopBar />
          <Container maxWidth="sm">
            <Switch>
              <Route exact path={PATHS.ABOUT_PATH}>
                <div>About</div>
              </Route>
              <Route path={`${PATHS.LEDGER_PATH}/:id/budget/add`} component={Budget} />
              <Route path={`${PATHS.LEDGER_PATH}/:id/add/:type`} component={Transact} />
              <Route path={`${PATHS.LEDGER_PATH}/:id`} component={Ledger} />
              <Route exact path={`${PATHS.HARVEST_PATH}/:id`} component={Harvest} />
              <Route exact path={PATHS.DASHBOARD_PATH} component={Dashboard} />
              <Route exact path={PATHS.NEW_LEDGER_PATH} component={NewLedger} />
              <Route exact path={PATHS.HOME_PATH} component={Home} />
            </Switch>
          </Container>
        </Router>
      </AuthContext.Provider>
    </ThemeProvider>
  );
}
export default connect(state => ({
	authContext: state.authContext,
}))(App);