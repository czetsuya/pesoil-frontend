/**
 * @author Edward P. Legaspi
 * @version 0.0.1
 */
import authContext from './modules/Authorization';

export default {
  authContext
}