import Dispatch from '../Dispatch';
import LedgerService from '../../api-services/Ledgers';
import { SUCCESS } from '../../models/Response';

const LIST = 'ledgers/LIST';
const CREATE = 'ledgers/CREATE';

const initialState = {
	list: [],
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case Dispatch.loadingAction(LIST): {
			return {
				...state
			};
		}
		default:
			return state;
	}
}

export const list = () => dispatch => {
	Dispatch.loading(dispatch, LIST);
	LedgerService.list()
		.then(response => {
			Dispatch.done(dispatch, LIST, response);
		});
};

export const create = (userAccount) => dispatch => {
	Dispatch.loading(dispatch, CREATE);
	LedgerService.create(userAccount)
		.then(result => {
			Dispatch.done(dispatch, CREATE, result);
		});
};