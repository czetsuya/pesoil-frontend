/**
 * @author Edward P. Legaspi
 * @version 0.0.1
 */
import { createStore, applyMiddleware, compose } from 'redux'
import { persistCombineReducers } from 'redux-persist'
import localForage from 'localforage'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from './Reducers'
import { monitorReducerEnhancer } from './Enhancers'

const isProductionMode = process.env.NODE_ENV && process.env.NODE_ENV.trim() === 'production';

let middleware = null

const middlewares = [thunk, promise]

if (isProductionMode) {
	middleware = compose(applyMiddleware(...middlewares))

} else {
	middleware = composeWithDevTools(
		applyMiddleware(...middlewares),
		monitorReducerEnhancer
	)
}

const configureStore = preloadedState => {
	const store = createStore(
		persistCombineReducers(
			{
				key: 'pesoil.org',
				storage: localForage,
				whitelist: ['authContext']
			},
			rootReducer,
		),
		preloadedState,
		middleware
	)

	return store
}

export default configureStore()